from celery import Celery
import time

# backend = 'db+scheme://username:password@host:port/dbname'
app = Celery('tasks', broker='amqp://localhost//', backend='db+mysql://root:root@localhost/python_apps')

@app.task
def reverse(string):
    time.sleep(5)
    return string[::-1]
