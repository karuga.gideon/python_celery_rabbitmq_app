from flask import Flask
from flask_celery import make_celery
from flask import jsonify 

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] ='amqp://localhost//'
app.config['CELERY_BACKEND'] = 'db+mysql://root:root@localhost/python_apps'

# celery = make_celery(app)

@app.route ('/process/<name>')
def process(name):
    # return name
    return jsonify(
        name=name,
        status="00"
    )

# @celery.task(name='celery_flask_eg.reverse')
def reverse(string):
    return string[::-1]


if __name__ == '__main__':
    app.run(debug=True)

# celery -A celery_flask_eg worker --loglevel=info
